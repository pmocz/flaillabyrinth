package com.imageliquid.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.input.GestureDetector;
import com.imageliquid.gameworld.GameRenderer;
import com.imageliquid.gameworld.GameWorld;
import com.imageliquid.helpers.InputHandler;

public class GameScreen implements Screen {

    private GameWorld world;
    private GameRenderer renderer;
    private float runTime;

    // This is the constructor, not the class declaration
    public GameScreen() {

        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        float gameWidth = 270; // internal game units
        float gameHeight = screenHeight / (screenWidth / gameWidth);

        int midPointX = (int) (gameWidth / 2);
        int midPointY = (int) (gameHeight / 2);

        world = new GameWorld();
        renderer = new GameRenderer(world, (int) gameWidth, (int) gameHeight, midPointX, midPointY);

        Gdx.input.setInputProcessor(new GestureDetector(20.f, 0.4f, 0.4f, 0.15f, new InputHandler(world, gameWidth, gameHeight, midPointX, midPointY)));

    }

    @Override
    public void render(float delta) {
        runTime += delta;
        world.update(delta);
        renderer.render(runTime, delta);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Gdx.app.log("GameScreen", "show called");
    }

    @Override
    public void hide() {
        Gdx.app.log("GameScreen", "hide called");
    }

    @Override
    public void pause() {
        Gdx.app.log("GameScreen", "pause called");
    }

    @Override
    public void resume() {
        Gdx.app.log("GameScreen", "resume called");
    }

    @Override
    public void dispose() {
        // Leave blank
    }

}