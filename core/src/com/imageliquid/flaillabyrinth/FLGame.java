package com.imageliquid.flaillabyrinth;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.imageliquid.screens.GameScreen;
import com.imageliquid.helpers.AssetLoader;

public class FLGame extends Game {
    
    @Override
    public void create() {
        System.out.println("FLGame Created!");
        AssetLoader.load();
        setScreen(new GameScreen());
    }
    
    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }
}
