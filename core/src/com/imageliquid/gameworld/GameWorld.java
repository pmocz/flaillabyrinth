package com.imageliquid.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Intersector;
import com.imageliquid.gameobjects.Cancer;
import com.imageliquid.gameobjects.End;
import com.imageliquid.gameobjects.Hero;
import com.imageliquid.gameobjects.Flail;
import com.imageliquid.gameobjects.Point;
import com.imageliquid.gameobjects.Wall;
import com.imageliquid.helpers.AssetLoader;

public class GameWorld {
	private Hero hero;
	private Flail flail;
	private Point[] point;
	private Wall[] wall;
	private Cancer[] cancer;
	private End end;

	private GameState currentState; // 0,1,2 for locked, unlocked-not-won, won
	private int levelSelected;

	public enum GameState {
		START, MENU, CONVERSATION, LEVEL
	}

	private FileHandle file;
	private String level;
	private String conversation;
	int cLoc;
	boolean isConversationDone;
	int conversationType;  // 0 start, 1 end

	private int mapHeight;
	private int mapWidth;
	private String mapChar = "@#p&c";
	private int[] mapCharCount = new int[mapChar.length()];

	// constructor
	public GameWorld() {
		levelSelected = 0;
		currentState = GameState.START;
	}

	private static int[] ind2sub(int m, int n, int idx) {
		int j = idx % (n+1);
		int i = (idx-j) / (n+1);
		int[] A = {j,i};
		return A;
	}

	// MAIN GAME LOOP
	public void update(float delta) {
		switch (currentState) {
		case START:
			break;
		case MENU:
			break;
		case CONVERSATION:
			break;
		case LEVEL:
			checkCollision(delta);

			// update hero, flail, enemies
			hero.update(delta);
			flail.update(delta, hero.getX(), hero.getY());
			for (int i = 0; i < getNCancer(); i++) 
				cancer[i].update(delta);

			checkCollision(delta);
			checkCollisionEnemies(delta);

			break;
		default:
			break;
		}

	}

	private void checkCollision(float delta) {
		// check collisions with walls
		for (int i = 0; i < getNWall(); i++) {
			if(Intersector.overlaps(hero.getBoundingCircle(), getWall(i).getBoundingRectangle())) {
				hero.handleCollision(delta, getWall(i).getBoundingCircle());
				hero.handleCollision(delta, getWall(i).getBoundingRectangle());
			}
			if(Intersector.overlaps(flail.getBoundingCircle(), getWall(i).getBoundingRectangle())) {
				flail.handleCollision(delta, getWall(i).getBoundingRectangle());
			}
		}

		// handle point pickup
		for (int i = 0; i < getNPoint(); i++) {
			if( !getPoint(i).isCollected() ) {
				if(Intersector.overlaps(hero.getBoundingCircle(), getPoint(i).getBoundingRectangle()) || 
						Intersector.overlaps(flail.getBoundingCircle(), getPoint(i).getBoundingRectangle())) {
					getPoint(i).Collect();
					hero.collectHP();
				}
			}
		}

		// check whether level is Won
		if( (hero.getX()-end.getX())*(hero.getX()-end.getX()) + (hero.getY()-end.getY())*(hero.getY()-end.getY()) < 100) {
			setLevelWon(levelSelected);
			unlockNewLevels(levelSelected);
			startConversation(levelSelected, 1);
		}
	}

	private void checkCollisionEnemies(float delta) {
		// check collision with enemies


		// enemy movement
		// check collisions with walls
		for (int i = 0; i < getNWall(); i++) {
			for (int j = 0; j < getNCancer(); j++) {
				if (getCancer(j).isAlive()) {
					if(Intersector.overlaps(getCancer(j).getBoundingCircle(), getWall(i).getBoundingRectangle())) {
						getCancer(j).changeDirection();
					}
				}
			}
		}
	}


	private void unlockNewLevels(int lvl) {
		// unlock adjacent levels that are not already unlocked
		int i = lvl-1;
		if( i >= 0 && i < 100 && AssetLoader.getLevelState(i) != '2') AssetLoader.setLevelState(i, '1');
		i = lvl-10;
		if( i >= 0 && i < 100 && AssetLoader.getLevelState(i) != '2') AssetLoader.setLevelState(i, '1');
		i = lvl+1;
		if( i >= 0 && i < 100 && AssetLoader.getLevelState(i) != '2') AssetLoader.setLevelState(i, '1');
		i = lvl+10;
		if( i >= 0 && i < 100 && AssetLoader.getLevelState(i) != '2') AssetLoader.setLevelState(i, '1');

	}

	public int getMapHeight() {
		return mapHeight;
	}

	public int getMapWidth() {
		return mapWidth;
	}

	public Hero getHero() {
		return hero;
	}

	public Flail getFlail() {
		return flail;
	}

	public Point getPoint(int i) {
		return point[i];
	}

	public int getNPoint() {
		for (int j = 0; j < mapChar.length(); j++) {
			if(mapChar.charAt(j) == 'p') {
				return mapCharCount[j];
			}
		}
		return 0;
	}

	public Wall getWall(int i) {
		return wall[i];
	}

	public int getNWall() {
		for (int j = 0; j < mapChar.length(); j++) {
			if(mapChar.charAt(j) == '#') {
				return mapCharCount[j];
			}
		}
		return 0;
	}

	public Cancer getCancer(int i) {
		return cancer[i];
	}

	public int getNCancer() {
		for (int j = 0; j < mapChar.length(); j++) {
			if(mapChar.charAt(j) == 'c') {
				return mapCharCount[j];
			}
		}
		return 0;
	}

	public End getEnd() {
		return end;
	}


	public boolean isStart() {
		return currentState == GameState.START;
	}

	public boolean isMenu() {
		return currentState == GameState.MENU;
	}

	public boolean isConversation() {
		return currentState == GameState.CONVERSATION;
	}

	public boolean isLevel() {
		return currentState == GameState.LEVEL;
	}

	public void selectLevel(int i) {
		levelSelected = i;
	}

	public int getLevelSelected() {
		return levelSelected;
	}

	public void goToMenu() {
		currentState = GameState.MENU;
	}

	public boolean isLevelWon(int i) {
		if(AssetLoader.getLevelState(i) == '2') {
			return true;
		} else {
			return false;
		}
	}

	public void setLevelWon(int i) {
		AssetLoader.setLevelState(i, '2');
		Gdx.app.log("GameWorld", "won level "+Integer.toString(i));
	}

	public boolean isLevelUnlockedAndNotWon(int i) {
		if(AssetLoader.getLevelState(i) == '1') {
			return true;
		} else {
			return false;
		}
	}

	public boolean isLevelUnlocked(int i) {
		if(AssetLoader.getLevelState(i) == '1' || AssetLoader.getLevelState(i) == '2') {
			return true;
		} else {
			return false;
		}
	}

	public boolean isLevelLocked(int i) {
		if(AssetLoader.getLevelState(i) == '0') {
			return true;
		} else {
			return false;
		}
	}

	public void startConversation(int lvl, int cType) {
		currentState = GameState.CONVERSATION;
		// read conversation text file
		if(cType == 0) {
			file = Gdx.files.internal("data/text/txt" + Integer.toString(lvl) + "start.txt");
		} else {
			file = Gdx.files.internal("data/text/txt" + Integer.toString(lvl) + "end.txt");
		}
		conversation = file.readString();
		cLoc = 0;
		isConversationDone = false;
		conversationType = cType;
	}

	public void progressConversation() {
		for (int i = cLoc; i < conversation.length(); i++){
			char c = conversation.charAt(i); 
			cLoc++;
			if (c == '\n') break;
		}
	}

	public int getConversationType() {
		return conversationType;
	}

	public String getNextLineConversation() {
		String cLine = "";
		if(cLoc == conversation.length()) isConversationDone = true;
		for (int i = cLoc; i < conversation.length(); i++){
			char c = conversation.charAt(i); 
			cLine += c;
			if (c == '\n') break;
		}
		return cLine;
	}

	public boolean isConversationDone() {
		return isConversationDone;
	}

	public void startLevel(int lvl) {
		// read level map, process & generate map
		file = Gdx.files.internal("data/levels/lvl" + Integer.toString(lvl) + ".txt");
		level = file.readString();

		// get dimensions and number of each objects of map
		mapHeight = 0;
		mapWidth = 0;
		for (int j = 0; j < mapChar.length(); j++) mapCharCount[j] = 0;
		for (int i = 0; i < level.length(); i++){
			char c = level.charAt(i);    
			for (int j = 0; j < mapChar.length(); j++) {
				if( c == mapChar.charAt(j) ) {
					mapCharCount[j]++;
				}
			}
			if ( c == '\n' && mapWidth == 0) {
				mapWidth = i;
			}
		}
		mapHeight = level.length() / (mapWidth + 1);
		Gdx.app.log("GameWorld", Integer.toString(level.length())+" "+Integer.toString(mapHeight)+" "+Integer.toString(mapWidth));

		// init object arrays
		for (int j = 0; j < mapChar.length(); j++) {
			switch (mapChar.charAt(j)) {
			case '#':
				wall = new Wall[mapCharCount[j]];
				break;
			case 'p':
				point = new Point[mapCharCount[j]];
				break;
			case 'c':
				cancer = new Cancer[mapCharCount[j]];
				break;
			}
			mapCharCount[j] = 0;
		}

		// create objects
		int[] sub = new int[2];
		for (int i = 0; i < level.length(); i++){
			char c = level.charAt(i);    
			sub = ind2sub(mapHeight, mapWidth, i);
			for (int j = 0; j < mapChar.length(); j++) {
				if( c == mapChar.charAt(j) ){
					switch (c) {
					case '@': 
						hero = new Hero(sub[0]*32, sub[1]*32, 32, 32); // size 32 game units
						flail = new Flail(sub[0]*32, sub[1]*32, 24, 24);
						break;
					case '#':
						wall[mapCharCount[j]] = new Wall(sub[0]*32, sub[1]*32, 32, 32);
						mapCharCount[j]++;
						break;
					case 'c':
						cancer[mapCharCount[j]] = new Cancer(sub[0]*32, sub[1]*32, 32, 32);
						mapCharCount[j]++;
						break;
					case 'p':
						point[mapCharCount[j]] = new Point(sub[0]*32, sub[1]*32, 8, 8); 
						mapCharCount[j]++;
						break;
					case '&':
						end = new End(sub[0]*32, sub[1]*32, 32, 32);
						break;
					default:
						break;
					}
				}
			}
		}
		currentState = GameState.LEVEL;
	}




}
