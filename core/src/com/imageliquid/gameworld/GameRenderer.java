package com.imageliquid.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.imageliquid.gameobjects.Hero;
import com.imageliquid.gameobjects.Flail;
import com.imageliquid.gameobjects.Point;
import com.imageliquid.gameobjects.Wall;
import com.imageliquid.gameobjects.End;
import com.imageliquid.helpers.AssetLoader;
import com.badlogic.gdx.utils.NumberUtils;

public class GameRenderer {

	private GameWorld myWorld;
	private OrthographicCamera cam;
	private ShapeRenderer shapeRenderer;

	private SpriteBatch batcher;

	private int midPointX;
	private int midPointY;
	private int gameWidth;
	private int gameHeight;

	private float camFocusX;
	private float camFocusY;

	private float[] chainX = new float[7];
	private float[] chainY = new float[7];
	float chainScale;

	// Game Assets
	private TextureRegion bg_tr, flail_tr, chain_tr, wall_tr, back_tr, end_tr;
	private Animation hero_a, cancer_a;

	public GameRenderer(GameWorld world, int gameWidth, int gameHeight, int midPointX, int midPointY) {
		myWorld = world;

		this.gameWidth = gameWidth;
		this.gameHeight = gameHeight;
		this.midPointX = midPointX;
		this.midPointY = midPointY;
		this.camFocusX = 0;
		this.camFocusY = 0;

		cam = new OrthographicCamera();
		cam.setToOrtho(true, gameWidth, gameHeight);

		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(cam.combined);
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);

		// Call helper methods to initialize instance variables
		initAssets();
	}



	private void drawLevel(float runTime, float delta) {


		// have a little delay between character movement and camera
		camFocusX = camFocusX + (myWorld.getHero().getX()-camFocusX)*delta*8.0f;
		camFocusY = camFocusY + (myWorld.getHero().getY()-camFocusY)*delta*8.0f;

		batcher.setProjectionMatrix(cam.combined);
		shapeRenderer.setProjectionMatrix(cam.combined);

		shapeRenderer.begin(ShapeType.Filled);

		// Draw Background color
		shapeRenderer.setColor(55 / 255.0f, 80 / 255.0f, 100 / 255.0f, 1);
		shapeRenderer.rect(0, 0, gameWidth, gameHeight);		

		shapeRenderer.end();

		batcher.begin();
		batcher.disableBlending();
		// draw background
		for (int i = 0; i < myWorld.getMapWidth(); i++) {
			for (int j = 0; j < myWorld.getMapHeight(); j++) {
				batcher.draw(AssetLoader.ground_tr, 
						midPointX + i*32.0f - camFocusX - 32.0f/2.0f,
						midPointY + j*32.0f - camFocusY - 32.0f/2.0f, 
						32, 32);
			}
		}

		//batcher.draw(bg, 0, midPointY + 23, gameWidth, gameHeight);
		// draw wall
		for (int i = 0; i < myWorld.getNWall(); i++) {
			batcher.draw(wall_tr, 
					midPointX + myWorld.getWall(i).getX() - camFocusX - myWorld.getWall(i).getWidth()/2.0f,
					midPointY + myWorld.getWall(i).getY() - camFocusY - myWorld.getWall(i).getHeight()/2.0f, 
					myWorld.getWall(i).getWidth(),
					myWorld.getWall(i).getHeight());
		}

		// draw end tile
		batcher.draw(end_tr, 
				midPointX + myWorld.getEnd().getX() - camFocusX - myWorld.getEnd().getWidth()/2.0f,
				midPointY + myWorld.getEnd().getY() - camFocusY - myWorld.getEnd().getHeight()/2.0f, 
				myWorld.getEnd().getWidth(),
				myWorld.getEnd().getHeight());

		batcher.enableBlending();  // (transparency switched on)

		// draw hero
		batcher.draw(hero_a.getKeyFrame(runTime), 
				midPointX + myWorld.getHero().getX() - camFocusX - myWorld.getHero().getWidth()/2.0f,
				midPointY + myWorld.getHero().getY() - camFocusY - myWorld.getHero().getHeight()/2.0f, 
				myWorld.getHero().getWidth(),
				myWorld.getHero().getHeight());

		//draw Enemies - cancer
		for (int i = 0; i < myWorld.getNCancer(); i++) {
			if (myWorld.getCancer(i).isAlive()) {
				batcher.draw(cancer_a.getKeyFrame(runTime), 
						midPointX + myWorld.getCancer(i).getX() - camFocusX - myWorld.getCancer(i).getWidth()/2.0f,
						midPointY + myWorld.getCancer(i).getY() - camFocusY - myWorld.getCancer(i).getHeight()/2.0f, 
						myWorld.getCancer(i).getWidth(),
						myWorld.getCancer(i).getHeight());
			}
		}

		// draw chains	
		chainX[0] = myWorld.getHero().getX() + myWorld.getFlail().getHandShiftX();
		chainY[0] = myWorld.getHero().getY() + myWorld.getFlail().getHandShiftY();
		for(int i=1; i<6; i++){
			chainX[i] = myWorld.getFlail().getAnchorX(i-1);
			chainY[i] = myWorld.getFlail().getAnchorY(i-1);
		}
		chainX[6] = myWorld.getFlail().getX();
		chainY[6] = myWorld.getFlail().getY();

		for(int i=0; i<6; i++){
			chainScale =  (float) (Math.sqrt((float) (chainX[i+1] - chainX[i]) * (chainX[i+1] - chainX[i]) + (chainY[i+1] - chainY[i]) * (chainY[i+1] - chainY[i])) / myWorld.getFlail().getChainHeight());
			batcher.draw(chain_tr, 
					midPointX + chainX[i] - camFocusX - myWorld.getFlail().getChainWidth()/2.0f,
					midPointY + chainY[i] - camFocusY - myWorld.getFlail().getChainWidth()/2.0f, 
					myWorld.getFlail().getChainWidth()/2.0f,
					0.0f,
					myWorld.getFlail().getChainWidth(),
					myWorld.getFlail().getChainHeight(),
					1.0f, chainScale, 180.0f / 3.14159f * MathUtils.atan2(chainY[i+1] - chainY[i], chainX[i+1] - chainX[i]) - 90.0f);
		}


		// draw flail head
		batcher.draw(flail_tr, 
				midPointX + myWorld.getFlail().getX() - camFocusX - myWorld.getFlail().getWidth()/2.0f,
				midPointY + myWorld.getFlail().getY() - camFocusY - myWorld.getFlail().getHeight()/2.0f, 
				myWorld.getFlail().getWidth() / 2.0f,
				myWorld.getFlail().getHeight() / 2.0f,
				myWorld.getFlail().getWidth(),
				myWorld.getFlail().getHeight(),
				1.0f, 1.0f, myWorld.getFlail().getRotation());

		batcher.end();

		shapeRenderer.begin(ShapeType.Filled);

		// Draw point 
		shapeRenderer.setColor(255 / 255.0f, 80 / 255.0f, 27 / 255.0f, 1);
		for (int i = 0; i < myWorld.getNPoint(); i++) {
			if( !myWorld.getPoint(i).isCollected() ) {
				shapeRenderer.rect(midPointX + myWorld.getPoint(i).getX() - camFocusX - myWorld.getPoint(i).getWidth()/2.0f, 
						midPointY + myWorld.getPoint(i).getY() - camFocusY - myWorld.getPoint(i).getHeight()/2.0f, 
						myWorld.getPoint(i).getWidth(), 
						myWorld.getPoint(i).getHeight());
			}
		}

		// draw HP
		shapeRenderer.setColor(255 / 255.0f, 80 / 255.0f, 27 / 255.0f, 1);
		for (int i = 0; i < myWorld.getHero().getHP(); i++) {
			shapeRenderer.rect(i*18+2,2,16,16);
		}
		shapeRenderer.setColor(55 / 255.0f, 55 / 255.0f, 55 / 255.0f, 1);
		for (int i = myWorld.getHero().getHP(); i < myWorld.getHero().getHPMax(); i++) {
			shapeRenderer.rect(i*18+2,2,16,16);
		}

		shapeRenderer.end();

		batcher.begin();
		// draw back arrow
		batcher.draw(back_tr, 2.0f*midPointX -34.0f, 2.0f, 32.0f, 32.0f);
		batcher.end();

	}








	public void render(float runTime, float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if (myWorld.isStart()) {
			drawStart(runTime);
		} else if (myWorld.isMenu()) {
			drawMenu(runTime);
		} else if (myWorld.isConversation()) {
			drawConversation();
		} else if (myWorld.isLevel()) {
			drawLevel(runTime, delta);
		} 

	}

	private void drawConversation() {
		batcher.begin();
		batcher.enableBlending(); 
		AssetLoader.font.draw(batcher, myWorld.getNextLineConversation(), 15, 320, 240, -1, true);
		batcher.end();
	}



	private void drawMenu(float runTime) {
		// Draw Background color
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(55 / 255.0f, 80 / 255.0f, 100 / 255.0f, 1);
		shapeRenderer.rect(0, 0, gameWidth, gameHeight);
		shapeRenderer.end();

		batcher.begin();
		batcher.disableBlending(); 
		batcher.draw(bg_tr, 200.0f*runTime % gameWidth - gameWidth, (-200.0f*runTime) % gameWidth, gameWidth*3, gameWidth*3);
		batcher.end();

		// draw levels
		shapeRenderer.begin(ShapeType.Filled);
		int ls = myWorld.getLevelSelected();
		shapeRenderer.setColor(255 / 255.0f, 255 / 255.0f, 0 / 255.0f, 1);
		shapeRenderer.rect(30*(ls % 10),280-30*(ls/10)-3,30,30);
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				int lvl = j*10 + i;
				if(myWorld.isLevelLocked(lvl)) {
					shapeRenderer.setColor(55 / 255.0f, 55 / 255.0f, 55 / 255.0f, 1);
					shapeRenderer.rect(30*i+3,280-30*j,24,24);
				}
				if(myWorld.isLevelUnlockedAndNotWon(lvl)) {
					shapeRenderer.setColor(100 / 255.0f, 180 / 255.0f, 180 / 255.0f, 1);
					shapeRenderer.rect(30*i+3,280-30*j,24,24);
				}
				if(myWorld.isLevelWon(lvl)) {
					shapeRenderer.setColor(100 / 255.0f, 100 / 255.0f, 180 / 255.0f, 1);
					shapeRenderer.rect(30*i+3,280-30*j,24,24);
				}
			}
		}
		shapeRenderer.setColor(55 / 255.0f, 55 / 255.0f, 55 / 255.0f, 1);
		shapeRenderer.rect(3,310,midPointX*2-6,132);
		if(myWorld.isLevelUnlockedAndNotWon(ls) ) {
			shapeRenderer.setColor(100 / 255.0f, 180 / 255.0f, 180 / 255.0f, 1);
		}
		if(myWorld.isLevelWon(ls) ) {
			shapeRenderer.setColor(100 / 255.0f, 100 / 255.0f, 180 / 255.0f, 1);
		}
		if(myWorld.isLevelLocked(ls) ) {
			shapeRenderer.setColor(55 / 255.0f, 55 / 255.0f, 55 / 255.0f, 1);
		}
		shapeRenderer.rect(6,313,midPointX*2-12,126);
		shapeRenderer.end();

		batcher.begin();
		batcher.enableBlending(); 
		if(myWorld.isLevelUnlocked(ls) ) {
			AssetLoader.bigFont.draw(batcher, "Start Lv " + Integer.toString(ls), midPointX - 120, 400);
		}
		if(myWorld.isLevelWon(ls) ) {
			AssetLoader.blueFont.draw(batcher, "won!", 10, 390);
		}
		if(myWorld.isLevelLocked(ls) ) {
			AssetLoader.blueFont.draw(batcher, "LOCKED!", 10, 390);
		}
		batcher.end();

	}



	private void drawStart(float runTime) {
		// Draw Background color
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(55 / 255.0f, 80 / 255.0f, 100 / 255.0f, 1);
		shapeRenderer.rect(0, 0, gameWidth, gameHeight);
		shapeRenderer.end();

		batcher.begin();
		batcher.disableBlending(); 
		batcher.draw(bg_tr, 100.0f*runTime % gameWidth - gameWidth, 100.0f*runTime % gameWidth - gameWidth, gameWidth*3, gameWidth*3);
		batcher.enableBlending(); 
		batcher.draw(flail_tr, 
				midPointX-64.0f/2.0f,
				midPointY-64.0f/2.0f, 
				64.0f,
				64.0f);
		AssetLoader.giantFont.draw(batcher, "Flail", midPointX - 70, 80);
		AssetLoader.bigFont.draw(batcher, "Labyrinth", midPointX - 80, 150);
		AssetLoader.blueFont.draw(batcher, "touch to start", midPointX - 70, 340);
		AssetLoader.smallFont.draw(batcher, "a game by ImageLiquid (C)", midPointX - 90, 420);
		batcher.end();
	}


	private void initAssets() {
		bg_tr = AssetLoader.bg_tr;
		flail_tr = AssetLoader.flail_tr;
		chain_tr = AssetLoader.chain_tr;
		wall_tr = AssetLoader.wall_tr;
		end_tr = AssetLoader.end_tr;
		back_tr = AssetLoader.back_tr;
		hero_a = AssetLoader.hero_a;
		cancer_a = AssetLoader.cancer_a;
	}
}