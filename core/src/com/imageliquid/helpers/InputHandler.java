package com.imageliquid.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.imageliquid.gameobjects.Hero;
import com.imageliquid.gameworld.GameWorld;

public class InputHandler implements GestureListener {

	private GameWorld myWorld;
	private float midPointX, midPointY;
	private OrthographicCamera cam;

	// Ask for a reference to the Hero when InputHandler is created.
	public InputHandler(GameWorld gameworld, float gameWidth, float gameHeight, float midPointX, float midPointY) {
		this.myWorld = gameworld;
		this.midPointX = midPointX;
		this.midPointY = midPointY;
		cam = new OrthographicCamera();
		cam.setToOrtho(true, gameWidth, gameHeight);
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		if (myWorld.isStart()) {
			myWorld.goToMenu();
		} else if (myWorld.isMenu()) {
			Vector3 input = new Vector3(x, y, 0);
			cam.unproject(input);
			if( input.y > 310 ) {
				int lvl = myWorld.getLevelSelected();
				if(myWorld.isLevelUnlocked(lvl)) {
					if( myWorld.isLevelUnlockedAndNotWon(lvl) ) {
						myWorld.startConversation(lvl, 0);
					}
					else {
						myWorld.startLevel(lvl);
					}
				}
			} else {
				myWorld.selectLevel(getClosestLevel(input.x, input.y));
			}
		} else if (myWorld.isConversation()) {
			myWorld.progressConversation();
			if( myWorld.isConversationDone() ) {
				if(myWorld.getConversationType() == 0) {
					myWorld.startLevel(myWorld.getLevelSelected());
				} else {
					myWorld.goToMenu();
				}

			}
		} else if (myWorld.isLevel()) {

		} 
		return true;
	}

	private int getClosestLevel(float x, float y) {
		int closestLevel = 0;
		float dist2 = (x-3-15)*(x-3-15) + (y-280-15)*(y-280-15);
		for(int i = 0; i < 10; i++) {
			for(int j = 0; j < 10; j++) {
				float dist2b = (x-(30*i+3+15))*(x-(30*i+3+15)) + (y-(280-30*j+15))*(y-(280-30*j+15));
				if(dist2b < dist2) {
					dist2 = dist2b;
					closestLevel = j*10 + i;
				}
			}
		}
		return closestLevel;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false; 
	}

	@Override
	public boolean longPress(float x, float y) {
		if (myWorld.isStart()) {
		} else if (myWorld.isMenu()) {
		} else if (myWorld.isConversation()) {
		} else if (myWorld.isLevel()) {
			Vector3 input = new Vector3(x, y, 0);
			cam.unproject(input);
			if( input.y < 48.0f && input.x > 2.0f * midPointX - 48.0f ) {
				myWorld.goToMenu();
			}
		} 
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		if (myWorld.isStart()) {

		} else if (myWorld.isMenu()) {
			Vector3 input = new Vector3(x, y, 0);
			cam.unproject(input);
			myWorld.selectLevel(getClosestLevel(input.x, input.y));
		} else if (myWorld.isConversation()) {

		} else if (myWorld.isLevel()) {
			Vector3 input = new Vector3(x, y, 0);
			cam.unproject(input);
			myWorld.getHero().move(input.x - midPointX, input.y - midPointY);
		} 
		return true;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		if (myWorld.isStart()) {

		} else if (myWorld.isMenu()) {

		} else if (myWorld.isConversation()) {

		} else if (myWorld.isLevel()) {
			myWorld.getHero().move(0, 0);
		} 
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2,
			Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

}
