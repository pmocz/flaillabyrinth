package com.imageliquid.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetLoader {

    private static Texture flail_t, chain_t, hero_t, wall_t, bg_t, ground_t, back_t, end_t, cancer_t;
    public static TextureRegion flail_tr, chain_tr, heroA_tr, heroB_tr, heroC_tr, wall_tr, bg_tr, ground_tr, back_tr, end_tr, cancerA_tr, cancerB_tr, cancerC_tr;
    public static BitmapFont font, bigFont, giantFont, smallFont, shadow, blueFont;
    public static Animation hero_a, cancer_a;
    
    public static Preferences prefs;
    

    public static void load() {

    	// textures
    	
    	flail_t = new Texture(Gdx.files.internal("data/flail.png"));
    	flail_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        flail_tr = new TextureRegion(flail_t, 0, 0, 32, 32);
        flail_tr.flip(false, true);
    	
        
    	chain_t = new Texture(Gdx.files.internal("data/chain.png"));
    	chain_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	chain_tr = new TextureRegion(chain_t, 0, 0, 8, 32);
    	chain_tr.flip(false, true);
    	
    	
    	hero_t = new Texture(Gdx.files.internal("data/hero.png"));
    	hero_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	heroA_tr = new TextureRegion(hero_t, 0, 0, 32, 32);
    	heroA_tr.flip(false, true);
    	
    	heroB_tr = new TextureRegion(hero_t, 32, 0, 32, 32);
    	heroB_tr.flip(false, true);
    	
    	heroC_tr = new TextureRegion(hero_t, 64, 0, 32, 32);
    	heroC_tr.flip(false, true);
    	
        TextureRegion[] heroFrames = { heroA_tr, heroB_tr, heroC_tr, heroB_tr };
        hero_a = new Animation(0.2f, heroFrames);
        hero_a.setPlayMode(Animation.PlayMode.LOOP);
        
        
    	cancer_t = new Texture(Gdx.files.internal("data/cancer.png"));
    	cancer_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	cancerA_tr = new TextureRegion(cancer_t, 0, 0, 32, 32);
    	cancerA_tr.flip(false, true);
    	
    	cancerB_tr = new TextureRegion(cancer_t, 32, 0, 32, 32);
    	cancerB_tr.flip(false, true);
    	
    	cancerC_tr = new TextureRegion(cancer_t, 64, 0, 32, 32);
    	cancerC_tr.flip(false, true);
    	
        TextureRegion[] cancerFrames = { cancerA_tr, cancerB_tr, cancerA_tr, cancerC_tr };
        cancer_a = new Animation(0.2f, cancerFrames);
        cancer_a.setPlayMode(Animation.PlayMode.LOOP);
    	
    	
    	wall_t = new Texture(Gdx.files.internal("data/wall.png"));
    	wall_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	wall_tr = new TextureRegion(wall_t, 0, 0, 32, 32);
    	wall_tr.flip(false, true);
    	
    	bg_t = new Texture(Gdx.files.internal("data/bg.png"));
    	bg_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	bg_tr = new TextureRegion(bg_t, 0, 0, 360, 360);
    	bg_tr.flip(false, true);
    	
    	ground_t = new Texture(Gdx.files.internal("data/ground.png"));
    	ground_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	ground_tr = new TextureRegion(ground_t, 0, 0, 32, 32);
    	ground_tr.flip(false, true);
    	
    	back_t = new Texture(Gdx.files.internal("data/back.png"));
    	back_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	back_tr = new TextureRegion(back_t, 0, 0, 32, 32);
    	back_tr.flip(false, true);
    	
    	end_t = new Texture(Gdx.files.internal("data/end.png"));
    	end_t.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
    	
    	end_tr = new TextureRegion(end_t, 0, 0, 32, 32);
        end_tr.flip(false, true);
    	
    	// Fonts
    	
		font = new BitmapFont(Gdx.files.internal("data/font/text.fnt"));
		font.getData().setScale(.25f, -.25f);
		
		bigFont = new BitmapFont(Gdx.files.internal("data/font/text.fnt"));
		bigFont.getData().setScale(.5f, -.5f);
		
		giantFont = new BitmapFont(Gdx.files.internal("data/font/text.fnt"));
		giantFont.getData().setScale(1.f, -1.f);
		
		smallFont = new BitmapFont(Gdx.files.internal("data/font/text.fnt"));
		smallFont.getData().setScale(.2f, -.2f);

		blueFont = new BitmapFont(Gdx.files.internal("data/font/bluetext.fnt"));
		blueFont.getData().setScale(.25f, -.25f);

		shadow = new BitmapFont(Gdx.files.internal("data/font/shadow.fnt"));
		shadow.getData().setScale(.25f, -.25f);
		
		// savefile
		
        // Create (or retrieve existing) preferences file
        prefs = Gdx.app.getPreferences("FlailLabyrinth");

        // Provide default state
        if (!prefs.contains("levelState")) {
            prefs.putString("levelState", "1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
            prefs.flush();
            Gdx.app.log("AssetLoader", "No gamesave found, starting new one");
        }

    }
    
    public static char getLevelState(int lvl) {
    	return prefs.getString("levelState").charAt(lvl);
    }
    
    public static void setLevelState(int lvl, char levelState) {
    	StringBuilder allStates= new StringBuilder( prefs.getString("levelState") );
    	allStates.setCharAt(lvl, levelState);
    	prefs.putString("levelState", allStates.toString());
    	prefs.flush();
    }

    public static void dispose() {
        // We must dispose of the texture when we are finished.
    	flail_t.dispose();
    	chain_t.dispose();
    	hero_t.dispose();
    	wall_t.dispose();
    	bg_t.dispose();
    	ground_t.dispose();
    	back_t.dispose();
    	end_t.dispose();
    	cancer_t.dispose();
    	
        font.dispose();
        bigFont.dispose();
        giantFont.dispose();
        smallFont.dispose();
        blueFont.dispose();
        shadow.dispose();
    }

}
