package com.imageliquid.gameobjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Cancer {

	private Vector2 position;
	private float velocity;
	private int hp;

	private int width;
	private int height;

	private Circle boundingCircle;

	public Cancer(float x, float y, int width, int height) {
		this.width = width;
		this.height = height;
		position = new Vector2(x, y);
		boundingCircle = new Circle();
		boundingCircle.set(position.x, position.y,  width/2.0f);
		this.velocity = 40.0f;
		this.hp = 3;
	}	

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}    

	public Circle getBoundingCircle() {
		return boundingCircle;
	}

	public void changeDirection() {
		velocity *= -1.0;
	}

	public void update(float delta) {
		position.x += velocity*delta;
		boundingCircle.set(position.x, position.y, width / 2.0f);
	}

	public boolean isAlive() {
		return hp > 0;
	}
}
