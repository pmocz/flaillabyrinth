package com.imageliquid.gameobjects;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Circle;

public class Hero {

	private Vector2 position;
	private Vector2 velocity;
	private Vector2 acceleration;

	private float rotation, prevRot; // For handling hero rotation
	private int width;
	private int height;

	private float vmax = 300.0f;

	private Circle boundingCircle;
	
	private int hp;
	private int hpMax = 10;

	// constructor
	public Hero(float x, float y, int width, int height) {
		this.width = width;
		this.height = height;
		position = new Vector2(x, y);
		velocity = new Vector2(0, 0);
		acceleration = new Vector2(0, 0);
		boundingCircle = new Circle();
		rotation = 0;
		hp = 3;
	}

	public void update(float delta) {

		velocity.add(acceleration.cpy().scl(delta));

		velocity.x = velocity.x - delta * velocity.x; // dissipate velocity slowly
		velocity.y = velocity.y - delta * velocity.y;

		float vnorm = velocity.len();
		if (vnorm > vmax) {
			velocity.x = velocity.x * vmax / vnorm;
			velocity.y = velocity.y * vmax / vnorm;
		}

		position.add(velocity.cpy().scl(delta));

		boundingCircle.set(position.x, position.y, width / 2.0f - 2.0f);

		// set rotation
		prevRot = rotation;
		rotation = 180.0f / 3.14159f * MathUtils.atan2(velocity.y, velocity.x) + 90.0f;
		if ( prevRot-rotation >  180.0f ) prevRot -= 360.0f;
		if ( prevRot-rotation < -180.0f ) prevRot += 360.0f;
		rotation = 0.9f*prevRot + 0.1f*rotation; // smooths rotation

	}

	public void move(float x, float y) {
		//float suppress = 1.0f;
		acceleration.x = 80.0f*x; 
		acceleration.y = 80.0f*y;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getRotation() {
		return rotation;
	}

	public Circle getBoundingCircle() {
		return boundingCircle;
	}

	
	public void handleCollision(float delta, Circle bcObj) {
		Vector2 circ = new Vector2(bcObj.x, bcObj.y);
		Vector2 diff = new Vector2(position.x - bcObj.x, position.y - bcObj.y);
		float dist = circ.dst(position);
		if ( dist < bcObj.radius + boundingCircle.radius ) {
			position.x = bcObj.x + (bcObj.radius + boundingCircle.radius) * diff.x /dist;
			position.y = bcObj.y + (bcObj.radius + boundingCircle.radius) * diff.y /dist;
		}
	}
	
	public void handleCollision(float delta, Rectangle boundingRectangle) {
		Vector2 start = new Vector2();
		Vector2 end = new Vector2();
		Vector2 displacement = new Vector2();

		start.x = boundingRectangle.x;
		start.y = boundingRectangle.y + boundingRectangle.height;
		end.x = boundingRectangle.x + boundingRectangle.width;
		end.y = boundingRectangle.y + boundingRectangle.height;
		
		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			position.add(displacement);
			velocity.y = 0;
			acceleration.y = 0;
		}

		start.x = boundingRectangle.x;
		start.y = boundingRectangle.y;
		end.x = boundingRectangle.x + boundingRectangle.width;
		end.y = boundingRectangle.y;

		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			position.add(displacement);
			velocity.y = 0;
			acceleration.y = 0;
		}

		start.x = boundingRectangle.x + boundingRectangle.width;
		start.y = boundingRectangle.y;
		end.x = boundingRectangle.x + boundingRectangle.width;
		end.y = boundingRectangle.y + boundingRectangle.height;

		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			position.add(displacement);
			velocity.x = 0;
			acceleration.x = 0;
		}

		start.x = boundingRectangle.x;
		start.y = boundingRectangle.y;
		end.x = boundingRectangle.x;
		end.y = boundingRectangle.y + boundingRectangle.height;

		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			position.add(displacement);
			velocity.x = 0;
			acceleration.x = 0;
		}
	}
	
	public int getHP() {
		return hp;
	}
	
	public int getHPMax() {
		return hpMax;
	}
	
	public void collectHP() {
		if(hp < hpMax) hp++;
	}

}
