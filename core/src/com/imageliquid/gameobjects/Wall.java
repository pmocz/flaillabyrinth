package com.imageliquid.gameobjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Wall {

    private Vector2 position;
    
    private int width;
    private int height;
    
    private Rectangle boundingRectangle;
    private Circle boundingCircle;

    public Wall(float x, float y, int width, int height) {
        this.width = width;
        this.height = height;
        position = new Vector2(x, y);
        boundingRectangle = new Rectangle();
        boundingRectangle.set(position.x - width/2.0f, position.y - height/2.0f, width, height);
        boundingCircle = new Circle();
        boundingCircle.set(position.x, position.y,  width/2.0f);
    }	
	
    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
    
    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }    
    
    public Rectangle getBoundingRectangle() {
    	return boundingRectangle;
    }
    
    public Circle getBoundingCircle() {
    	return boundingCircle;
    }
    
}
