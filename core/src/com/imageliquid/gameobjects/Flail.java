package com.imageliquid.gameobjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.MathUtils;

public class Flail {

	private Vector2 position;
	private Vector2 velocity;
	private Vector2 acceleration;
	private float handShiftX = 10;
	private float handShiftY = 14;

	private float vmax = 1600.0f;

	private float rotation; // For handling flail rotation
	private int width;
	private int height;

	private Circle boundingCircle;

	private float[] anchorX = new float[5];
	private float[] anchorY = new float[5];
	private float[] anchorVX = new float[5];
	private float[] anchorVY = new float[5];
	private float[] anchorAX = new float[5];
	private float[] anchorAY = new float[5];
	private float[] chainX = new float[7];
	private float[] chainY = new float[7];
	private float[] chainVX = new float[7];
	private float[] chainVY = new float[7];
	private float chainLength = 1.0f;
	private float springConst = 4000.0f;
	private float damping = 0.5f;


	// constructor
	public Flail(float x, float y, int width, int height) {
		this.width = width;
		this.height = height;
		position = new Vector2(x+handShiftX, y+handShiftY);
		velocity = new Vector2(0, 0);
		acceleration = new Vector2(0, 0);

		boundingCircle = new Circle();

		anchorX[0] = x+handShiftX - chainLength;
		anchorY[0] = y+handShiftY; 
		anchorX[1] = x+handShiftX - chainLength;
		anchorY[1] = y+handShiftY - chainLength;
		anchorX[2] = x+handShiftX;
		anchorY[2] = y+handShiftY - chainLength;
		anchorX[3] = x+handShiftX + chainLength;
		anchorY[3] = y+handShiftY - chainLength;
		anchorX[4] = x+handShiftX + chainLength;
		anchorY[4] = y+handShiftY;
		for(int i=0; i<5; i++){
			anchorVX[i] = 0;
			anchorVY[i] = 0;
			anchorAX[i] = 0;
			anchorAY[i] = 0;
		}
	}

	public void update(float delta, float heroX, float heroY) {

		// clear spring accelerations
		for(int i=0; i<5; i++){
			anchorAX[i] = 0;
			anchorAY[i] = 0;
		}
		chainX[0] = heroX + handShiftX;
		chainY[0] = heroY + handShiftY;
		chainVX[0] = anchorVX[0];
		chainVY[0] = anchorVY[0];
		for(int i=1; i<6; i++){
			chainX[i] = anchorX[i-1];
			chainY[i] = anchorY[i-1];
			chainVX[i] = anchorVX[i-1];
			chainVY[i] = anchorVY[i-1];
		}
		chainX[6] = position.x;
		chainY[6] = position.y;
		chainVX[6] = velocity.x;
		chainVY[6] = velocity.y;

		// calculate spring accelerations
		for(int i=0; i<5; i++){
			float dxL = chainX[i] - chainX[i+1];
			float dyL = chainY[i] - chainY[i+1];
			float dxR = chainX[i+2] - chainX[i+1];
			float dyR = chainY[i+2] - chainY[i+1];
			float dL = (float) (Math.sqrt( dxL * dxL + dyL * dyL));
			float dR = (float) (Math.sqrt( dxR * dxR + dyR * dyR));
			float diffL = dL - chainLength;
			float diffR = dR - chainLength;
			float fL = springConst * diffL;
			float fR = springConst * diffR;
			float dpL = dxL * (chainVX[i  ] - chainVX[i+1]) + dyL * (chainVY[i  ] - chainVY[i+1]);
			float dpR = dxR * (chainVX[i+2] - chainVX[i+1]) + dyR * (chainVY[i+2] - chainVY[i+1]);
			if(Math.abs(diffL) > chainLength/1000.0f) {
				anchorAX[i] += 0.5f*( fL + damping*dpL/chainLength ) * dxL/dL;
				anchorAY[i] += 0.5f*( fL + damping*dpL/chainLength ) * dyL/dL;
			}
			if(Math.abs(diffR) > chainLength/1000.0f) {
				anchorAX[i] += 0.5f*( fR + damping*dpR/chainLength ) * dxR/dR;
				anchorAY[i] += 0.5f*( fR + damping*dpR/chainLength ) * dyR/dR;
			}
			if(i==4) {
				acceleration.x = -0.5f*( fR + damping*dpR/chainLength ) * dxR/dR;
				acceleration.y = -0.5f*( fR + damping*dpR/chainLength ) * dyR/dR;
			}
		}

		// update velocities
		float vnorm = velocity.len();
		if (vnorm > vmax) {
			velocity.x = velocity.x * vmax / vnorm;
			velocity.y = velocity.y * vmax / vnorm;
		}

		velocity.add(acceleration.cpy().scl(delta));
		velocity.x = velocity.x - 0.2f * delta * velocity.x; // dissipate velocity slowly
		velocity.y = velocity.y - 0.2f * delta * velocity.y;
		for(int i=0; i<5; i++){
			anchorVX[i] += delta * anchorAX[i] - 0.00f * delta * anchorVX[i];
			anchorVY[i] += delta * anchorAY[i] - 0.00f * delta * anchorVY[i];
		}

		// update positions
		position.add(velocity.cpy().scl(delta));
		for(int i=0; i<5; i++){
			anchorX[i] += delta * anchorVX[i];
			anchorY[i] += delta * anchorVY[i];
		}

		boundingCircle.set(position.x, position.y, width / 2.0f - 2.0f);

		// set rotation
		rotation = 180.0f / 3.14159f * MathUtils.atan2(position.x - anchorX[4], position.y - anchorY[4]) - 90.0f;


		// fix flail if it exploded
		if(position.x < -100000 || position.x > 100000 || position.y < -100000 || position.y > 100000 || position.x!=position.x || position.y !=position.y) {
			position.x = heroX + handShiftX;
			position.y = heroY + handShiftY;
			velocity.x = 0;
			velocity.y = 0;
			acceleration.x = 0;
			acceleration.y = 0;

			boundingCircle.set(position.x, position.y, width / 2.0f - 2.0f);

			anchorX[0] = heroX+handShiftX - chainLength;
			anchorY[0] = heroY+handShiftY; 
			anchorX[1] = heroX+handShiftX - chainLength;
			anchorY[1] = heroY+handShiftY - chainLength;
			anchorX[2] = heroX+handShiftX;
			anchorY[2] = heroY+handShiftY - chainLength;
			anchorX[3] = heroX+handShiftX + chainLength;
			anchorY[3] = heroY+handShiftY - chainLength;
			anchorX[4] = heroX+handShiftX + chainLength;
			anchorY[4] = heroY+handShiftY;
			for(int i=0; i<5; i++){
				anchorVX[i] = 0;
				anchorVY[i] = 0;
				anchorAX[i] = 0;
				anchorAY[i] = 0;
			}
		}



	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getRotation() {
		return rotation;
	}

	public float getChainWidth() {
		return width/4.0f;
	}

	public float getChainHeight() {
		return height;
	}

	public float getChainLength() {
		return chainLength;
	}

	public float getAnchorX(int i) {
		return anchorX[i];
	}

	public float getAnchorY(int i) {
		return anchorY[i];
	}

	public Circle getBoundingCircle() {
		return boundingCircle;
	}

	public float getHandShiftX() {
		return handShiftX;
	}

	public float getHandShiftY() {
		return handShiftY;
	}

	// TODO: make a collidable object class
	public void handleCollision(float delta, Rectangle boundingRectangle) {
		Vector2 start = new Vector2();
		Vector2 end = new Vector2();
		Vector2 displacement = new Vector2();

		start.x = boundingRectangle.x;
		start.y = boundingRectangle.y + boundingRectangle.height;
		end.x = boundingRectangle.x + boundingRectangle.width;
		end.y = boundingRectangle.y + boundingRectangle.height;

		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			//position.add(displacement);
			velocity.y = -0.5f*velocity.y;
			//acceleration.y = 0;
		}

		start.x = boundingRectangle.x;
		start.y = boundingRectangle.y;
		end.x = boundingRectangle.x + boundingRectangle.width;
		end.y = boundingRectangle.y;

		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			//position.add(displacement);
			velocity.y = -0.5f*velocity.y;
			//acceleration.y = 0;
		}

		start.x = boundingRectangle.x + boundingRectangle.width;
		start.y = boundingRectangle.y;
		end.x = boundingRectangle.x + boundingRectangle.width;
		end.y = boundingRectangle.y + boundingRectangle.height;

		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			//position.add(displacement);
			velocity.x = -0.5f*velocity.x;
			//acceleration.x = 0;
		}

		start.x = boundingRectangle.x;
		start.y = boundingRectangle.y;
		end.x = boundingRectangle.x;
		end.y = boundingRectangle.y + boundingRectangle.height;

		if ( Intersector.intersectSegmentCircleDisplace(start, 
				end, 
				position, 
				boundingCircle.radius, 
				displacement) < Float.POSITIVE_INFINITY ){
			//position.add(displacement);
			velocity.x = -0.5f*velocity.x;
			//acceleration.x = 0;
		}


	}


}
