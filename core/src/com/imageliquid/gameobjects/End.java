package com.imageliquid.gameobjects;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class End {

    private Vector2 position;
    
    private int width;
    private int height;
    
    private Rectangle boundingRectangle;

    public End(float x, float y, int width, int height) {
        this.width = width;
        this.height = height;
        position = new Vector2(x, y);
        boundingRectangle = new Rectangle();
        boundingRectangle.set(position.x - width/2.0f, position.y - height/2.0f, width, height);
    }	
	
    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
    
    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }    
    
    public Rectangle getBoundingRectangle() {
    	return boundingRectangle;
    }
	
}
