package com.imageliquid.flaillabyrinth.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.imageliquid.flaillabyrinth.FLGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Flail Labyrinth";
        config.width = 540;
        config.height = 960;
		new LwjglApplication(new FLGame(), config);
	}
}
